#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <UniversalTelegramBot.h>
 
// Initialize Wifi connection to the router
char ssid[] = "XXXXXXX";     //wifi user name
char password[] = "XXXXXXX"; //wifi pass
 
// Initialize Telegram BOT
#define BOTtoken "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"  //telegram bot token
 
WiFiClientSecure client;
UniversalTelegramBot bot(BOTtoken, client);
 
int Bot_mtbs = 1000;
long Bot_lasttime;  
bool Start = false;
 
const int relay1 = D1;
const int relay2 = D4;
int relaystatus = 0;
 
 
 
 
void handleNewMessages(int numNewMessages) {
  Serial.println("handleNewMessages");
  Serial.println(String(numNewMessages));
 
 
  for (int i=0; i<numNewMessages; i++) {
    String chat_id = String(bot.messages[i].chat_id);
    String text = bot.messages[i].text;
 
 
  if (chat_id != "199215559" &&chat_id != "96513484" ) {
    String message = "Chat-ID: " + chat_id + "\n";
    bot.sendMessage("199215559", message, "Markdown"); 
    return;
    }
 
    String from_name = bot.messages[i].from_name;
    if (from_name == "") from_name = "Guest";
 
     
    if (text == "/hi_itok") {
        String keyboardJson = "[[\"Up\", \"Up-stop\"],[\"Down\", \"Down-Stop\"]]";
        bot.sendMessageWithReplyKeyboard(chat_id, "Use Keyboard", "", keyboardJson, true);
        bot.sendChatAction(chat_id, "typing");
        delay(4000);
        bot.sendMessage(chat_id, "");
    }
 
 
          if (text == "Up") {
      digitalWrite(relay1, HIGH);   // turn the Relay on (HIGH is the voltage level)
      delay(20000);
      digitalWrite(relay1, LOW); // initialize pin as off
      relaystatus = 1;
      bot.sendMessage(chat_id, "Door is Open ... loading", "");
          }
 
       if (text == "Up-stop") {
      digitalWrite(relay1, LOW);    // turn the LED off (LOW is the voltage level)
      relaystatus = LOW;
      bot.sendMessage(chat_id, "Door Stoped!", "");
       }
 
 
 if (text == "Down") {
       digitalWrite(relay2, HIGH);   // turn the Relay on (HIGH is the voltage level)
      delay(20000);
      digitalWrite(relay2, LOW); // initialize pin as off
      relaystatus = 1;
      bot.sendMessage(chat_id, "Door is Close... loading", "");
 }
 
 if (text == "Down-Stop") {
      digitalWrite(relay2, LOW);    // turn the LED off (LOW is the voltage level)
      relaystatus = LOW;
      bot.sendMessage(chat_id, "Door Stoped", "");
       }
    
    if (text == "/start") {
      String welcome = "Welcom to Itok Door Opening System" + from_name + ".\n";
      welcome += "/hi_itok : This is ItokSmart IOT.\n\n";
      bot.sendMessage(chat_id, welcome);
    }
  }
}
 
 
void setup() {
  Serial.begin(115200);
 
  // Set WiFi to station mode and disconnect from an AP if it was Previously
  // connected
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);
 
  // attempt to connect to Wifi network:
  Serial.print("Connecting Wifi: ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
 
 
  pinMode(D1, OUTPUT); // initialize digital Relay as an output.
  digitalWrite(relay1, HIGH); // initialize pin as off
  delay(4000);
  digitalWrite(relay1, LOW); // initialize pin as off
 
 
  pinMode(D4, OUTPUT); // initialize digital Relay as an output.
  digitalWrite(relay2, HIGH); // initialize pin as off
  delay(4000);
  digitalWrite(relay2, LOW); // initialize pin as off
 
 
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
 
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}
 
void loop() {
  if (millis() > Bot_lasttime + Bot_mtbs)  {
    int numNewMessages = bot.getUpdates(bot.last_message_received + 1);
 
    while(numNewMessages) {
      Serial.println("got response");
      handleNewMessages(numNewMessages);
      numNewMessages = bot.getUpdates(bot.last_message_received + 1);
    }
 
    Bot_lasttime = millis();
  }
}
